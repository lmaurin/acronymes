#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Data Management related Acronym translator.
"""

__version__ = "September 23rd, 2022"
__author__ = "Loïc Maurin <loic.maurin@universite-paris-saclay.fr>"

from difflib import SequenceMatcher

# List of acronyms (+ metada)
ecal = """\
Title: Mes acronymes
Date: Sepetember 23rd, 2022

AIM: Astrophysics Instrumentation Modelisation (labo CEA)
AMIS: Astrophysics of Interstellar Matter (IAS)
AMQP: Advanced Message Queuing Protocol
BISOU: Balloon Interferometer for Spectral Observations of the primordial Universe (projet)
BYOPIC: BarYOn PIcture of the Cosmos (projet ERC) 
CARE: Collective benefit, Authority to control, Responsibility, and Ethics
CEA: Commissariat à l'Energie Atomique et aux Energies Alternatives 
CNES: Centre National d'Etudes Spatiales
CNRS: Centre National de la Recherche Scientifique
D2S: Stellar System Data (IDOC)
DIP: Dissemination Information Package
DMP: Data Management Plan
DOI: Digital Object Identifier
ESA: European Space Agency
FAIR: Findable, Accessible, Interoperable, Reusable
GEOPS: Géosciences Paris-Saclay (labo)
JUICE: JUpiter ICy moons Explorer (mission)
JWST: James Webb Space Telescope (mission)
IAS: Institut d'Astrophysique Spatiale
IDOC: Integrated Data and Operation Center (IAS)
IVOA: International Virtual Observatory Alliance
MAGYC: Multi wAvelength GalaxY Clusters
MAJIS: Moons And Jupiter Imaging Spectrometer (instrument JUICE)
MIRI: Mid InfraRed Instrument (instrument JWST)
MEDOC: Multi Experiment Data and Operation Center (IDOC)
PLATO: PLAnetary Transits and Oscillations of stars
PSA: Planetary Science Archive
PSUP: Planetary SUrface Portal (IDOC)
OAIS: Open Archival Information System
OSUPS: Observatoire des Sciences de l'Univers de l'Université Paris-Sud
REGARDS: REnouvellement des outils Génériques d'Accès et d'aRchivage pour les Données Spatiales
SIP: Simplified Ingestion Procedure
SZ: Sunyaev-Zeldovich effect
UWS: Universal Worker Service (IVOA standard)
VO: Virtual Observatory
""".split('\n')


def read_acronyms(lines):
    """
    Return acronym dictionary {ACRONYM:definition} from acronym lines.
    """

    from collections import OrderedDict

    d = OrderedDict()
    for line in lines:
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        try:
            key, val = line.split(':')       # ACRONYM: definition
        except ValueError:
            print(f"WARNING: cannot decipher line {line!r}")
            continue
        val = val.strip()
        if key not in d:
            d[key] = val
        elif val != d[key]:
            print(f"WARNING: merging {key} entries: '{val}' and '{d[key]}'")
            d[key] = ' | '.join((d[key], val))

    return d


def get_similarity(a, b):

    return SequenceMatcher(None, a, b).ratio()


if __name__ == '__main__':

    import argparse
    import re

    try:
        import rich
        def rprint(acronym, subdef, maxl=20):  # rich print out
            rich.print(f"[bold red]{acronym:>{maxl}}[/bold red]: {subdef}")
    except ImportError:
        def rprint(acronym, subdef, maxl=20):  # standard print out
            print(f"{acronym:>{maxl}}: {subdef}")

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('acronym', nargs='*')

    parser.add_argument("-E", "--regex", action="store_true",
                        help="Interpret arguments as regular expressions.",
                        default=False)
    parser.add_argument("-R", "--reversed", action="store_true",
                        help="Reversed look-up (always regex).",
                        default=False)
    parser.add_argument("-s", "--similarity", metavar="[0.0-1.0]", type=float,
                        help="Print similar keywords, given a match fraction.",
                        default=-1)
    parser.add_argument("--json", type=argparse.FileType('w'),
                        help="Dump list of acronyms to JSON file.")
    parser.add_argument('--version', action='version', version=__version__)

    args = parser.parse_args()

    d = read_acronyms(ecal)     # {acronym: definition}

    title = d.pop('Title')
    date = d.pop('Date')
    print(f"{title} ({date})\n")

    if args.json:
        import json

        jthing = [ dict(name=key, description=val)
                   for key, val in d.items() ]  # [{name: key, description: val}]

        json.dump(jthing, args.json,
                  ensure_ascii=False, indent=None, separators=(',', ':'))
        print("Acronym list saved to", args.json.name)
        args.json.close()

    maxl = max( len(key) for key in d )  # Length of longest acronym

    if not args.acronym:        # All acronyms
        acronyms = sorted(d.keys())
    elif args.regex:            # Matching acronyms
        acronyms = [ key for acronym in args.acronym
                     for key in d
                     if re.search(acronym, key) ]
    elif args.reversed:         # Acronyms which definition matches
        acronyms = [ key for acronym in args.acronym
                     for key, val in d.items()
                     if re.search(acronym, val, re.IGNORECASE) ]
    else:                       # Specific acronyms
        acronyms = args.acronym

    for acronym in acronyms:
        definition = d.get(acronym, 'Yet Another Unknown Acronym (YAUA)')
        for subdef in definition.split(' | '):
            rprint(acronym, subdef, maxl)

        if 0 <= args.similarity <= 1:
            similars = [ key for key in d
                         if get_similarity(key, acronym) > args.similarity ]
            if similars:
                print("\nSimilar acronyms:", " ".join(similars), "\n")
        elif definition.endswith('(YAUA)'):
            similars = [ key for key in d
                         if get_similarity(key, acronym) > 0.7 ]
            if similars:
                print("\nDid you mean:", " ".join(similars), "?\n")

# Local Variables:
# time-stamp-line-limit: 10
# End:
