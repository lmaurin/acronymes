# Acronymes

Ce projet permet de créer une page web listant tous les acronymes que j'utilise dans mon travail. Il est adapté du projet de Yannick Copin "The EUCLID Acronym translaTOR ([EUCLIDATOR](https://gitlab.euclid-sgs.uk/ycopin/euclidator))".

Pour ajouter un acronyme, ajoutez-le simplement à la liste dans `acronymator.py`.